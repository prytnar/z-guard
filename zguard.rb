require 'sinatra'
require './lib/config_data_mgr.rb'

configure do
	Config = ConfigDataMgr.load!
end

get '/' do
  erb :index
end

get '/toggle' do
	Config.armed = !Config.armed
  redirect '/'
end

get '/set/:status' do
	Config.armed = params[:status] == 'armed' ? true : false
  redirect '/'
end

get '/alert/:type' do
	if Config.armed
		c = Config.send(params[:type].to_s)

		case params[:type]
		when 'gmail'
			`curl --url "smtps://smtp.gmail.com:465" --ssl-reqd --mail-from "#{c[:login]}@gmail.com" --mail-rcpt "#{c[:login]}@gmail.com" --upload-file views/danger_email.txt --user "#{c[:login]}@gmail.com:#{c[:password]}" --insecure`
		when 'sms' 
			`curl --url "http://api.gsmservice.pl/send.php?login=#{c[:login]}&pass=#{c[:password]}&recipient=#{c[:recipient]}&text=ALARM%0B&type=2&sender=Z-Guard"`
		else
			# provide yours
		end
	else
		'no notification sent, currently disarmed !'
	end
end
