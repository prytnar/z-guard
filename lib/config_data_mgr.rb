require 'ostruct'
require 'yaml'
require 'forwardable'

class ConfigDataMgr
	extend Forwardable
	def_delegators :@data, :armed, :gmail, :sms
	attr_accessor :data

	def initialize yaml_path
		@yaml_path = yaml_path
	end

	def self.load!(yaml_path='config.yml')
		config = new(yaml_path)
		
		File.open(yaml_path, 'r') do |fd|
			config.data = OpenStruct.new(YAML.load(fd))
		end

		return config
	end

	def dump
		File.open(@yaml_path, 'w') do |fd|
			fd.write(YAML.dump(@data.to_h))
		end
	end

	def armed= bool_val
		@data.armed = bool_val
		dump
	end
end
